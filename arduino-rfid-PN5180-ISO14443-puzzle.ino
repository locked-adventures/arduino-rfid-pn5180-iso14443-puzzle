/**
 * Multi-PN5180 
 * An "escape room"-style puzzle in which a number of RFID tags must be placed in front 
 * of the correct PN5180 RFID readers in order to be detected and solve the puzzle.
 */

// DEFINES
// #define DEBUG 

// INCLUDES
// Download from https://github.com/playfultechnology/PN5180-Library
#include <PN5180.h>
#include <PN5180ISO14443.h>
#include <string.h>

// CONSTANTS
// The number of PN5180 readers connected
// static const byte numReaders = 4;
// What is the "correct" UID that should be detected by each reader
#define CARD_ALTERNATIVES 1
static const uint8_t correctUid[][CARD_ALTERNATIVES][8] = {
  {
    {0x04,0x19,0x68,0x52,0x2B,0x5D,0x81,0x00}
  }
  },
  {
    {0x04,0xFA,0x68,0x52,0x2B,0x5D,0x80,0x00}
  },
  {
    {0x04,0xDC,0x68,0x52,0x2B,0x5D,0x80,0x00}
  }
};
// This pin will be driven LOW when the puzzle is solved
static const byte relayPin = 10;

// GLOBALS
// Each PN5180 reader requires unique NSS, BUSY, and RESET pins,
// NSS and RESET are from Arduino to PN5180 and require logic conversion 5V -> 3.3V.
// RESET is from PN5180 to Arduino and does not require any logic conversion, because Arduino is 3.3V tolerant.
// as defined in the constructor below

// Order is SS, BUSY, RST
static PN5180ISO14443 nfc[] = {
  PN5180ISO14443(15, 14, 16), // works
  PN5180ISO14443(17, 19, 18), // works
  PN5180ISO14443(7, 2, 6), // works
//  PN5180ISO14443(3, 5, 4), // works
};
#define numReaders (sizeof(nfc)/sizeof(*nfc))
// Array to record the value of the last UID read by each reader

void setup() {
  // Configure the relay pin
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, HIGH);
  
  // Initialise serial connection
  Serial.begin(115200);

  Serial.print("Number of RFID readers: ");
  Serial.println(numReaders);

  for(int i=0; i<numReaders; i++){
    Serial.print("\r\n\r\nReader #");
    Serial.println(i);
    nfc[i].begin();
    
    Serial.println(F("----------------------------------"));
    Serial.println(F("PN5180 Hard-Reset..."));
    nfc[i].reset();
    
    Serial.println(F("----------------------------------"));
    Serial.println(F("Reading product version..."));
    uint8_t productVersion[2];
    nfc[i].readEEprom(PRODUCT_VERSION, productVersion, sizeof(productVersion));
    Serial.print(F("Product version="));
    Serial.print(productVersion[1]);
    Serial.print(".");
    Serial.println(productVersion[0]);
    
    if (0xff == productVersion[1]) { // if product version 255, the initialization failed
      Serial.println(F("Initialization failed!?"));
      Serial.println(F("Press reset to restart..."));
      Serial.flush();
      exit(-1); // halt
    }
    
    Serial.println(F("----------------------------------"));
    Serial.println(F("Reading firmware version..."));
    uint8_t firmwareVersion[2];
    nfc[i].readEEprom(FIRMWARE_VERSION, firmwareVersion, sizeof(firmwareVersion));
    Serial.print(F("Firmware version="));
    Serial.print(firmwareVersion[1]);
    Serial.print(".");
    Serial.println(firmwareVersion[0]);
    
    Serial.println(F("----------------------------------"));
    Serial.println(F("Reading EEPROM version..."));
    uint8_t eepromVersion[2];
    nfc[i].readEEprom(EEPROM_VERSION, eepromVersion, sizeof(eepromVersion));
    Serial.print(F("EEPROM version="));
    Serial.print(eepromVersion[1]);
    Serial.print(".");
    Serial.println(eepromVersion[0]);
    
    Serial.println(F("----------------------------------"));
    Serial.println(F("Enable RF field..."));
    nfc[i].setupRF();
  }
  Serial.println(F("Setup Complete"));
}

// static bool prevPuzzleSolved = false;

static uint32_t loopCnt = 0;
static bool errorFlag[numReaders] = {};
static uint8_t lastUid[5][8];

void loop() {
  Serial.println(F("----------------------------------"));
  Serial.print(F("\r\nLoop #"));
  Serial.println(loopCnt++);
  for (int i = 0; i<numReaders; i++) {
    Serial.print(F("Reader #"));
    Serial.println(i);
    if (!nfc[i].isCardPresent()) {
      Serial.println(F("no card found"));
      // memset(lastUid[i], 0, 8);
    }
    else {
      uint8_t uid[8] = {};
      int8_t len = nfc[i].readCardSerial(uid);
      if (!len) {
        Serial.print(F("Error in readCardSerial: "));
        uint32_t irqStatus = nfc[i].getIRQStatus();
        showIRQStatus(irqStatus);
    
        if (0 == (RX_SOF_DET_IRQ_STAT & irqStatus)) { // no card detected
          Serial.println(F("*** No card detected!"));
        }
    
        nfc[i].reset();
        nfc[i].setupRF();
    
        errorFlag[i] = false;
        delay(10);
      }
      else {
        Serial.print(F("card serial successful, len="));
        Serial.print(len);
        Serial.print(F(", UID="));
        char buf[32];
        for (int i=0; i<8; i++) {
          int len = snprintf(buf, 32, "%02X", uid[i]);
          if (len >= 0) {
            Serial.print(buf); 
            if (i < 2) Serial.print(":");
          }
        }
        Serial.println();
        // Serial.println(F("----------------------------------"));  
      }
    }
  }
  /* if (isPuzzleSolved()) {
    Serial.println("Solved");
    digitalWrite(relayPin, LOW);
  }
  else {
    Serial.println("Not solved yet");
    digitalWrite(relayPin, HIGH);
  } */
  
  delay(1000);
}

// Check whether all PN5180s have detected the correct tag
static bool isPuzzleSolved() {
  // Test each reader in turn
  for(int i=0; i<numReaders; i++){
    // If this reader hasn't detected the correct tag
    bool thisReaderCardCorrect = false;
    for (int j=0; j<CARD_ALTERNATIVES; j++) {
      if (!memcmp(lastUid[i], correctUid[i][j], 8)) {
        thisReaderCardCorrect = true;
        break;
      }
    }
    if (!thisReaderCardCorrect) {
      return false;
    }
  }
  return true;
}

static void showIRQStatus(uint32_t irqStatus) {
  Serial.print(F("IRQ-Status 0x"));
  Serial.print(irqStatus, HEX);
  Serial.print(": [ ");
  if (irqStatus & (1<< 0)) Serial.print(F("RQ "));
  if (irqStatus & (1<< 1)) Serial.print(F("TX "));
  if (irqStatus & (1<< 2)) Serial.print(F("IDLE "));
  if (irqStatus & (1<< 3)) Serial.print(F("MODE_DETECTED "));
  if (irqStatus & (1<< 4)) Serial.print(F("CARD_ACTIVATED "));
  if (irqStatus & (1<< 5)) Serial.print(F("STATE_CHANGE "));
  if (irqStatus & (1<< 6)) Serial.print(F("RFOFF_DET "));
  if (irqStatus & (1<< 7)) Serial.print(F("RFON_DET "));
  if (irqStatus & (1<< 8)) Serial.print(F("TX_RFOFF "));
  if (irqStatus & (1<< 9)) Serial.print(F("TX_RFON "));
  if (irqStatus & (1<<10)) Serial.print(F("RF_ACTIVE_ERROR "));
  if (irqStatus & (1<<11)) Serial.print(F("TIMER0 "));
  if (irqStatus & (1<<12)) Serial.print(F("TIMER1 "));
  if (irqStatus & (1<<13)) Serial.print(F("TIMER2 "));
  if (irqStatus & (1<<14)) Serial.print(F("RX_SOF_DET "));
  if (irqStatus & (1<<15)) Serial.print(F("RX_SC_DET "));
  if (irqStatus & (1<<16)) Serial.print(F("TEMPSENS_ERROR "));
  if (irqStatus & (1<<17)) Serial.print(F("GENERAL_ERROR "));
  if (irqStatus & (1<<18)) Serial.print(F("HV_ERROR "));
  if (irqStatus & (1<<19)) Serial.print(F("LPCD "));
  Serial.println("]");
}
